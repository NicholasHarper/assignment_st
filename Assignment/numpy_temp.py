# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 18:44:50 2018

@author: skitt
"""
#import the neccessary libraries
import openpyxl
import sqlite3
import numpy
import matplotlib.pyplot as plt


#Create function that searches through given list 
#Which averages numbers where years match
def meanF(result):
    newlst = []
    yearAVG = []
    previousYear = 0.0
    for row in result:
        currentYear = float(row[1])
        if currentYear == previousYear:
            newlst.append(float(row[0]))
        elif currentYear != previousYear and len(newlst) < 1:
            newlst.append(float(row[0]))
        else:
            yearAVG.append((numpy.mean(newlst), row[1]))
            newlst = []
        previousYear = float(row[1])
    return (yearAVG)

#Take the State to compare and the Australian Average and Compare where the years match
def compare(State, AA):
    complst = []
    datelst = []
    for i in range(0, len(State)):
        for j in range(0, len(AA)):
            if State[i][1] == AA[j][1]:
                value = AA[j][0] - State[i][0]
                complst.append(value)
                datelst.append(State[i][1])
    return(complst, datelst)
 
#Write the state's value and year into excel, columns are important    
def writing(col, value, year, State):
    for i in range(2, len(value)):
        sheet.cell(row = 1, column = col).value = State
        sheet.cell(row = 1, column = col + 1).value = "The Difference"
        sheet.cell(row = 1, column = col + 2).value = "Year of Change"
        sheet.cell(row = i, column = col + 1).value = str(value[i])
        sheet.cell(row = i, column = col + 2).value = str(year[i])

 #######################################################################               
#Try and open the file
try:
    wb = openpyxl.load_workbook('WorldTemperature.xlsx')
#If it doesn't exist create one
except:
    wb = openpyxl.Workbook('WorldTemperature.xlsx')
    
#Try and open the file
try:
    sheet = wb.get_sheet_by_name('Comparison')
#If it doesn't exist create one
except:
    sheet = wb.create_sheet(index = 1, title = 'Comparison')
    

#Connecting too / creating a database called Databases
connection = sqlite3.connect("Databases.db")
cursor = connection.cursor()
########################################################################
#Search through and gather the average temperature for Queensland
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE state = "Queensland"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Queensland")
Queensland = meanF(result)
#print(len(Queensland))
########################################################################

########################################################################
#Search through and gather the average temperature for ACT
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "A%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("ACT")
ACT = meanF(result)
#print(len(ACT))
#######################################################################

#######################################################################
#Search through and gather the average temperature for NSW
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "N%S%W%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("NSW")
NSW = meanF(result)
#print(len(NSW))
#######################################################################


#######################################################################
#Search through and gather the average temperature for Northern Territory
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "N%y"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Northern Territory")
NT = (meanF(result))
#print(len(NT))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Western Australia
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "W%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Western Australia")
WA = meanF(result)
#print(len(WA))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Tasmania
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "T%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Tasmania")
Tas = meanF(result)
#print(len(Tas))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Victoria
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "V%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Victoria")
Victoria = meanF(result)
#print(len(Victoria))
#######################################################################

#######################################################################
#Search through and gather the average temperature for South Australia
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltState
WHERE country = "Australia" AND state Like "S%A%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("South Australia")
SA = meanF(result)
#print(len(SA))
######################################################################

#######################################################################
#Search through and gather the average temperature for each year in Austrlia
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS 'Year'
FROM gltCountry
WHERE country = "Australia"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print("Australian Average")
AA = meanF(result)
#print(len(AA))
######################################################################
#Run Compare function on each state
######################################################################
compQLD = compare(Queensland, AA)
compACT = compare(ACT, AA)
compNSW = compare(NSW, AA)
compNT = compare(NT, AA)
compWA = compare(WA, AA)
compTas = compare(Tas, AA)
compVic = compare(Victoria, AA)
compSA = compare(SA, AA)
#####################################################################
#Plotting the data to a graph
#####################################################################
fig, ax = plt.subplots(1,1)
plt.plot(compTas[1], compTas[0], "y-",
         compACT[1], compACT[0], "b-",
         compVic[1], compVic[0], "c-",
         compNSW[1], compNSW[0], "k-",  
         compSA[1], compSA[0], "g-",
         compWA[1], compWA[0], "m-",
         compQLD[1], compQLD[0], "r-",
         compNT[1], compNT[0], "k-")
         

plt.legend(["Tas", "ACT", "Vic", "NSW", "SA", "WA", "QLD","NT"], bbox_to_anchor=(1.21, 0.8))
plt.xlabel("Year")
plt.ylabel("Temperature")
plt.title("State Temperature vs Country Temperature")
for label in ax.get_xticklabels()[::]:
    label.set_visible(False)
for label in ax.get_xticklabels()[::20]:
    label.set_visible(True)
plt.show()
######################################################################
#Writing The data into the excel (ACT)
######################################################################
writing(1, compTas[0], compTas[1], "TAS")
writing(4, compACT[0], compACT[1], "ACT")
writing(7, compVic[0], compVic[1], "Vic")
writing(10, compNSW[0], compNSW[1], "NSW")
writing(13, compSA[0], compSA[1], "SA")
writing(16, compQLD[0], compQLD[1], "QLD")
writing(19, compWA[0], compWA[1], "WA")
writing(22, compNT[0], compNT[1], "NT")



#Save the excel document and close the database connection
wb.save('WorldTemperature.xlsx')
connection.close()