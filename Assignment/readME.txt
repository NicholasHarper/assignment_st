Welcome to the ReadMe file for s5093543's python scripts
Order scripts should be run:
1) db_create.py
2) sql_temp.py
3) excel_temp.py
4) numpy_temp.py

db_create requires three local files. 
- GlobalLandTemperaturesByCountry.xlsx
- GlobalLandTemperaturesByMajorCity.xlsx
- GlobalLandTemperaturesByState.xlsx
This file create a database filled with three tables, one for each excel file.
Excel entries with incomplete data sets were not entered and instead ignored.

sql_temp only requires a connection to the database.
This file creates a new table in the database and fills it 
with queried data from the other tables.


excel_temp only requires a connection to the database, however it does still 
require a connectionto an excel file that it creates.
This file creates a new excel file called WorldTemperature. A new sheet is created and
the average temperature for each Chinese city and for each year is queried.
This information is then written into the created excel file and sheet.

numpy_temp requires many different python libraires. This script requires
the WorldTemperature file and the database.
This script creates another worksheet in the excel file and a graph
that graphs the difference of Australian state yearly averages against 
the Austrlaian year average.
