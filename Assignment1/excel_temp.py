# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 09:41:00 2018

@author: Nicholas
"""
#Import all neccessary libraries
import openpyxl
import sqlite3

#Try and open the file
try:
    wb = openpyxl.load_workbook('WorldTemperature.xlsx')
#If it doesn't exist create one
except:
    wb = openpyxl.Workbook('WorldTemperature.xlsx')
    
#Try and open the file
try:
    sheet = wb.get_sheet_by_name('TemperatureByCity')
#If it doesn't exist create one
except:
    sheet = wb.create_sheet(index = 1, title = "TemperatureByCity")


#Connecting too / creating a database called Databases
connection = sqlite3.connect("Databases.db")
cursor = connection.cursor()

#Search through and gather the average temperature for each city in china
sql_command = """
SELECT AVG(avgTemp), country, city, strftime('%Y', date)
FROM gltCity
WHERE country = "China"
GROUP BY  strftime('%Y', date)
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print(result)
#Set up headings for all the 
sheet.cell(row = 1, column = 1).value = "Average Temperature"
sheet.cell(row = 1, column = 2).value = "Country"
sheet.cell(row = 1, column = 3).value = "City"
sheet.cell(row = 1, column = 4).value = "Year"

#Set up row variable
i = 2
#for each row of data, write to the created excel sheet
for row in result:
    print(row)
    sheet.cell(row = i, column = 1).value = str(row[0])
    sheet.cell(row = i, column = 2).value = str(row[1])
    sheet.cell(row = i, column = 3).value = str(row[2])
    sheet.cell(row = i, column = 4).value = str(row[3])
    i += 1

#Save excel sheet and close database connection
wb.save('WorldTemperature.xlsx')
connection.close()