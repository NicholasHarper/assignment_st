# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 18:44:50 2018

@author: skitt
"""

import openpyxl
import sqlite3
import numpy

def meanF(result):
    newlst = []
    yearAVG = []
    previousYear = 0.0
    for row in result:
        currentYear = float(row[1])
        if currentYear == previousYear:
            newlst.append(float(row[0]))
        elif currentYear != previousYear and len(newlst) < 1:
            newlst.append(float(row[0]))
        else:
            yearAVG.append((numpy.mean(newlst), row[1]))
            newlst = []
        previousYear = float(row[1])
    return (yearAVG)

def yearMean(data):
    newlst = []
    yearAVG = []
    previousYear = 0.0
    for row in result:
        currentYear = float(row[1])
        if currentYear == previousYear:
            newlst.append(float(row[0]))
        elif currentYear != previousYear and len(newlst) < 1:
            newlst.append(float(row[0]))
        else:
            yearAVG.append((numpy.mean(newlst), row[1]))
            newlst = []
        previousYear = float(row[1])
    return (yearAVG)

#Try and open the file
try:
    wb = openpyxl.load_workbook('WorldTemperature.xlsx')
#If it doesn't exist create one
except:
    wb = openpyxl.Workbook('WorldTemperature.xlsx')
    
#Try and open the file
try:
    sheet = wb.get_sheet_by_name('Comparison')
#If it doesn't exist create one
except:
    sheet = wb.create_sheet(index = 1, title = 'Comparison')
    

#Connecting too / creating a database called Databases
connection = sqlite3.connect("Databases.db")
cursor = connection.cursor()
########################################################################
#Search through and gather the average temperature for Queensland
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE state = "Queensland"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Queensland")
Queensland = meanF(result)
print(len(Queensland))
########################################################################

########################################################################
#Search through and gather the average temperature for ACT
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "A%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("ACT")
ACT = meanF(result)
print(len(ACT))
#######################################################################

#######################################################################
#Search through and gather the average temperature for NSW
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "N%S%W%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("NSW")
NSW = meanF(result)
print(len(NSW))
#######################################################################


#######################################################################
#Search through and gather the average temperature for Northern Territory
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "N%y"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Northern Territory")
NT = (meanF(result))
print(len(NT))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Western Australia
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "W%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Western Australia")
WA = meanF(result)
print(len(WA))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Tasmania
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "T%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Tasmania")
Tas = meanF(result)
print(len(Tas))
#######################################################################

#######################################################################
#Search through and gather the average temperature for Victoria
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "V%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Victoria")
Victoria = meanF(result)
print(len(Victoria))
#######################################################################

#######################################################################
#Search through and gather the average temperature for South Australia
sql_command = """
SELECT avgTemp, strftime('%Y', date) AS ' Year'
FROM gltState
WHERE country = "Australia" AND state Like "V%"
ORDER BY date ASC;"""

#Execute the above command
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("South Australia")
SA = meanF(result)
print(len(SA))
######################################################################
combinedLst = [Queensland,ACT, NSW, NT, WA, Tas, Victoria, SA]
combinedLst.sort()
sortedList = []
print(combinedLst)

wb.save('WorldTemperature.xlsx')
connection.close()