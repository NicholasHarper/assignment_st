# -*- coding: utf-8 -*-
"""
Created on Mon Jan  8 20:43:07 2018

@author: skitt
"""
#import the neccessary libraries
import sqlite3

#Connecting too / creating a database called Databases
connection = sqlite3.connect("Databases.db")
cursor = connection.cursor()

#Drop table to start new creating every time
cursor.execute("""DROP TABLE if exists southern_Cities;""")

#Setup of Table southern_Cities
sql_command = """
CREATE TABLE if not exists southern_Cities (
rowID INTEGER PRIMARY KEY,
city VARCHAR(15), 
country VARCHAR(15), 
lat VARCHAR(6),
long VARCHAR(6)); """
#Execute the above sql command
cursor.execute(sql_command)

#Search for cities in the southernhemisphere
sql_command = """
SELECT city, country, lat, long
FROM gltCity
WHERE lat LIKE '%S'
GROUP BY city; """
#Execute the SQL search
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
#print(result)

#Take all results and one by one instert them into the new table
for row in result:
    format_str = """INSERT INTO southern_Cities (rowID, city, country, lat, long) 
            VALUES (NULL, "{City}", "{Country}", "{Lat}", "{Long}");"""
    sql_command = format_str.format(City = row[0], Country = row[1], Lat = row[2], Long = row[3])
    cursor.execute(sql_command)
connection.commit()
#Show all from southern_Cities to show it worked
#cursor.execute("SELECT * FROM southern_Cities")
#result = cursor.fetchall()
#print(result)

#Search for min and max average Temperatures from Queensland
sql_command = """
SELECT MIN(avgTemp), MAX(avgTemp), date, state, country
FROM gltState
WHERE state = 'Queensland';
"""
#Execute the SQL search
cursor.execute(sql_command)
connection.commit()

#Show all results
result = cursor.fetchall()
print("Min, Max, Date, State, Country")
print(result)

#Close connection
connection.close()