# -*- coding: utf-8 -*-
"""
Created on Thu Dec 21 23:35:24 2017

@author: Nicholas
"""
#import the neccessary libraries
import sqlite3
import openpyxl as op
#############################################
#GlobalLandTemp By Counties

wb = op.load_workbook('GlobalLandTemperaturesByCountry.xlsx')
sheet = wb.get_sheet_by_name('GlobalLandTemperaturesByCountry')

#Connecting too / creating a database called Databases
connection = sqlite3.connect("Databases.db")
cursor = connection.cursor()

#Drop table to start new creating every time
cursor.execute("""DROP TABLE if exists gltCountry;""")

#Setup of Table gltCountry
sql_command = """
CREATE TABLE if not exists gltCountry (
rowID INTEGER PRIMARY KEY,
date DATE, 
avgTemp FLOAT(24), 
avgTempUnc FLOAT(24), 
country VARCHAR(15)); """
cursor.execute(sql_command)

#Inserting the data into a table
for rowcol in sheet.iter_rows(min_row = 2, max_row = sheet.max_row):
    #Excluding all records with a None in the avgTemp or TempUnc column
    if rowcol[2].value == None or rowcol[3].value == None:
        continue
    else:
        #Inserting all data for each row into the database
        format_str = """INSERT INTO gltCountry (rowID, date, avgTemp, avgTempUnc, country) 
            VALUES (NULL, "{date}", "{avgTemp}", "{TempUnc}", "{Country}");"""
        sql_command = format_str.format(date=rowcol[0].value, avgTemp=rowcol[1].value, TempUnc=rowcol[2].value, Country=rowcol[3].value)
        cursor.execute(sql_command)
connection.commit()

#cursor.execute("SELECT * FROM gltCountry") 
#result = cursor.fetchall()

#print("fetchall:", result)



#############################################
#GlobalLandTemp By Major Cities

wb = op.load_workbook('GlobalLandTemperaturesByMajorCity.xlsx')
sheet = wb.get_sheet_by_name('GlobalLandTemperaturesByMajorCi')

#Drop table to start new creating every time
cursor.execute("""DROP TABLE if exists gltCity;""")

#Setup of Table gltCity
sql_command = """
CREATE TABLE if not exists gltCity (
rowID INTEGER PRIMARY KEY,
date DATE, 
avgTemp FLOAT(24), 
avgTempUnc FLOAT(24), 
city VARCHAR(15),
country VARCHAR(15),
lat VARCHAR(6),
long VARCHAR(6)); """
cursor.execute(sql_command)

#Inserting the data into a table
for rowcol in sheet.iter_rows(min_row = 2, max_row = sheet.max_row):
    #Excluding all records with a None in the avgTemp or TempUnc column
    if rowcol[1].value == None or rowcol[2].value == None:
        continue
    else:
        #Inserting all data for each row into the database
        format_str = """INSERT INTO gltCity (rowID, date, avgTemp, avgTempUnc, city, country, lat, long) 
            VALUES (NULL, "{date}", "{avgTemp}", "{TempUnc}", "{City}", "{Country}", "{Lat}", "{Long}");"""
        sql_command = format_str.format(date=rowcol[0].value, avgTemp=rowcol[1].value, TempUnc=rowcol[2].value, 
                      City = rowcol[3].value, Country=rowcol[4].value, Lat = rowcol[5].value, Long = rowcol[6].value)
        cursor.execute(sql_command)
connection.commit()

#cursor.execute("SELECT * FROM gltCity") 
#result = cursor.fetchall()
#print("fetchall:", result)

#############################################
#GlobalLandTemp By States

wb = op.load_workbook('GlobalLandTemperaturesByState.xlsx')
sheet = wb.get_sheet_by_name('GlobalLandTemperaturesByState')

#Drop table to start new creating every time
#cursor.execute("""DROP TABLE gltState;""")

#Setup of Table gltState
sql_command = """
CREATE TABLE if not exists gltState (
rowID INTEGER PRIMARY KEY,
date DATE, 
avgTemp FLOAT(24), 
avgTempUnc FLOAT(24),
state VARCHAR(15), 
country VARCHAR(15)); """
cursor.execute(sql_command)


#Inserting the data into a table
for rowcol in sheet.iter_rows(min_row = 2, max_row = sheet.max_row):
    #Excluding all records with a None in the avgTemp or TempUnc column
    if rowcol[1].value == None or rowcol[2].value == None:
        continue
    else:
        #Inserting all data for each row into the database
        format_str = """INSERT INTO gltState (rowID, date, avgTemp, avgTempUnc, state, country) 
            VALUES (NULL, "{date}", "{avgTemp}", "{TempUnc}", "{State}", "{Country}");"""
        sql_command = format_str.format(date=rowcol[0].value, avgTemp=rowcol[1].value, 
                    TempUnc=rowcol[2].value, State = rowcol[3].value, Country=rowcol[4].value)
        cursor.execute(sql_command)
connection.commit()

#cursor.execute("SELECT * FROM gltState") 
#result = cursor.fetchall()
#print("fetchall:", result)

connection.close()
